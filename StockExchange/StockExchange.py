class InfoCollector:
    """It takes name of a company from stock market and we gets overall 
    Data from each especific function. At first create object of this class
    like =>variable_name = InfoCollector(Company_name) and then call specific
    function to get specific data like =>variable_name.market_info()"""
    
    def __init__(self, stockName):
        
        self.HTML = get_html(stockName)  
   
    def market_info(self):
        """It returns market information"""
        sop=data_table(self.HTML)
        string_list=filtering_string(sop[0]) 
        string_list= string_list[1:]                
        return get_market_info(string_list)
    
    def get_BasicInformation(self):
        """It returns Basic information"""
        sop=data_table(self.HTML)
        string_list=filtering_string(sop[1]) 
        return get_basic_data(string_list[1:])
    
    def lastGMA(self):
        """It returns data about last GMA"""
        sop=data_table(self.HTML)
        string_list=filtering_string(sop[2]) 
        return get_data_dict(string_list)
    
    
    def interim_financial_performance(self):
        """It returns data of interim financial performance"""
        import pandas as pd
        sop=data_table(self.HTML)
        string_list=filtering_string(sop[4])
        return pd.DataFrame(get_financial_performance(string_list[10:]),index=["Q1(3 months)","Q2(6 months)","Q3(9 months)","Q4(12 months)"]).T

    def profit_status(self):
        """It returns status of profit"""
        sop=data_table(self.HTML)
        string_list=filtering_string(sop[6])
        return get_profit_data(string_list[2:])
    
    
    def last_audited_financial(self):
        """It returns financial data thats last audited"""
        sop=data_table(self.HTML)
        string_list=filtering_string(sop[7])   
        return get_audited_data_dict(string_list)
    
    
    def get_financial_perforamace(self):
        """It returns data of financial performance"""
        import pandas as pd
        sop=data_table(self.HTML)
        string_list=filtering_string(sop[9])
        string_list2=filtering_string(sop[12])

        df1= pd.DataFrame(get_financial_data(string_list[14:]),index=["Basic EPS based on continuing operations","Basic EPS based on including extra ordinary income","Basic EPS (restated) based on continuing operations","Basic EPS based on (restated) including extra ordinary income","Net assest value per share"," Restated Net assest value per share","Net profit after tax(mn) continuing operations","Net profit after tax(mn) including extra ordinary income"])
        df2= pd.DataFrame(get_financial_data2(string_list2[7:]),index=["Year end P/E based on continuing operations","Year end P/E based on including extra ordinary income","%Dividend","%Dividend Yield"])
        return pd.concat([df1,df2]).T

    
    def finalcial_satatement(self):
        """It returns financial statement of a company."""
        sop=data_table(self.HTML)
        string_list=filtering_string(sop[14])   
        return get_data_dict(string_list)
    

    def otherInfo_of_the_company(self):
        """It returns more information about company."""
        sop=data_table(self.HTML)
        string_list=filtering_string(sop[15])
        dictionary=get_info_dict(string_list)
        return get_other_company_details(string_list,dictionary)  
    
    def address_of_company(self):
        """It returns specific location of a company"""
        sop=data_table(self.HTML)
        string_list=filtering_string(sop[16])   
        return get_data_dict(string_list[2:])
    
    
def get_html(stockName):
        web_add="http://www.dsebd.org/displayCompany.php?name="+stockName
        import requests
        return requests.get(web_add)

def data_table(HTML):
    from bs4 import BeautifulSoup
    sop2=[]
    soup2 = BeautifulSoup(HTML.content)
    sop=soup2.findAll('table' ,border="1")
    return sop


def filtering_string(list_of_table):
    string_list=[]
    
    for string in list_of_table.strings:
        if ((string== u'\xa0')):
            pass
        elif((string== u'\n')):
            pass
        elif((string== u'\n')):
            pass
        elif((string== u' \xa0')):
            pass
        elif((string== u' ')):
            pass
        elif((string== u'\xa0 ')):
            pass
        elif((string== u'\xa0 \r\n                    ')):
            pass
        elif((string== u'\xa0\r\n                  ')):
            pass
        else:
            string_list.append(string)
    return string_list
    
    
def get_basic_data(de):
    dctu={}
    for g in range(0,5):
        if(g%2==0):
            dctu[de[g]]=de[g+1]
    for g in range(6,len(de)):
        if(g==6):
            dctu[de[g]]="NaN"
        if(g%2!=0):
            dctu[de[g]]=de[g+1]   
    return dctu

def get_info_dict(de):
    p=[]
    for i in range(8,13):
        p.append(de[8])
        de.remove(de[8])
    de.remove(de[0])
    n={}
    for t in range(0,len(p)):
        if (t==1):
            z,q =p[t].split(".")
            n[z]=q
        else:    
            z,q =p[t].split(" ")
            n[z]=q
    return n

def get_other_company_details(de,n):
    dctu={}
    for g in range(0,5):
        if(g%2==0):
            dctu[de[g]]=de[g+1]



    for g in range(6,len(de)):
        if(g==6):
            dctu[de[g]]=n
        if(g==7):
            dctu[de[g]]=de[g+1]    
    return dctu

def get_data_dict(di):
    dictu={}
    for w in range(0,len(di)):
        if(w%2==0):
            dictu[di[w]]=di[w+1]
    return dictu    
        
def get_audited_data_dict(string_list):
    audited_data_dict={}
    for d in (0,len(string_list)):
        try:
            audited_data_dict[string_list[d]+string_list[d+1]]=string_list[d+2]
            string_list=string_list[3:]
        except:
            pass
    return audited_data_dict


def get_market_info(de):
    dctu={}
    for g in range(0,6):
        if(g%2==0):
            if(g==4):
                dctu[de[g]]=[de[g+1],de[g+2]]
            else:
                dctu[de[g]]=de[g+1]
        
    for g in range(7,len(de)):      
        if(g%2!=0):
            
            if(g==15):
                f=de[g+1].split("-")
                dctu[de[g]]=[f[0],f[1]]
            else:
                dctu[de[g]]=de[g+1]  
    return dctu


def get_financial_performance(string):
    
    dctu={}
    f=0
    for g in range(0,len(string)):
        
        if(g%5==0):
            
            if(g<=5):
                dctu[string[g]]=[string[g+1],string[g+2],string[g+3],string[g+4]]
            elif(g<=15):
                g=g+f
                dctu[string[g]+string[g+1]]=[string[g+2],string[g+3],string[g+4],string[g+5]]
                f=f+1
            elif(g>=20 and g<=30):
                if(g==25):
                    g=g+2
                    dctu[string[g]+string[g+1]]=[string[g+2],string[g+3],string[g+4],string[g+5]]
                else:
                    g=g+f
                    dctu[string[g]]=[string[g+1],string[g+2],string[g+3],string[g+4]]
                    f+=1
                
        
    return dctu  

def get_profit_data(string_list):
    audited_data_dict={}
    for d in (0,len(string_list)):
        try:
            audited_data_dict[string_list[d]+string_list[d+1]]=[string_list[d+2],"NaN"]
            string_list=string_list[4:]
        except:
            pass
    return audited_data_dict

def get_financial_data(string):
    financial_data={}
    for st in range(0,len(string)):
        if(st%9==0):
            financial_data[string[st]]=[string[st+1],string[st+2],string[st+3],string[st+4],string[st+5],string[st+6],string[st+7],string[st+8]]
    return financial_data   

def get_financial_data2(string):
    financial_data={}
    for st in range(0,len(string)):
        if(st%5==0):
            financial_data[string[st]]=[string[st+1],string[st+2],string[st+3],string[st+4]]
    return financial_data 
