
import setuptools

# TODO: add the constraints module because of the arrow alignment in the plots
setuptools.setup(author="Alok Mondal",
                 author_email="alokmondol@datarobin.com",
                 name="StockExchange",
                 description="Finding Data of DSE",
                 version="1dev",
                 packages=setuptools.find_packages(),
                 package_data={'': ['*.txt', '*.md']},
                 install_requires=[],
                 license="DataRobin",
                 platforms=['Linux'],
                 classifiers=[],
                 url="")
